
# RDF Schema

## IICU 2025.1 Dataset


This version of the schema used for the IICU NDS project is based on [SPHN 2025.1](https://www.biomedit.ch/rdf/sphn-schema/sphn/2025/1). The dataset, doc and schema directories are empty as no additional concepts are used.


Links to SPHN 2025.1:
 - [**dataset**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/pre-release-2025-1/templates/dataset_template/SPHN_dataset_template_pre_release_2025_1.xlsx)
 - [**doc**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/tree/pre-release-2025-1/dataset/documentation-2024.1)
 - [**schema**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/pre-release-2025-1/rdf_schema/sphn_rdf_schema.ttl)


 <!-- [Link to downloads of .TTL files, documentations, SHACL rules and QC/CC SPARQL of the SPHN dataset 2024-1 release](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/releases/2024-1).


## Subset used in this version of the DTR


The DTR includes the following concepts:  
**TO DO: Add list of concepts that are requested**


## Git clone

The first time you clone the directory, run the following command to also pull the linked version of the SPHN ontology. It will be placed into rdf-schema/version-1/sphn-ontology
```shell
git submodule update --init
``` -->


